/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hangmangame;

import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author basem
 */
public class Hangman implements HangmanInterface {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
     private int numOfWords;
     private String [] dictionary;
     private String gameWord;
     private char[] toDisplay;
     private int allowedWrongs=10;
     private boolean win;
     private boolean lose;
     private int rights;
     //private int wrongs;
     @SuppressWarnings("FieldMayBeFinal")
     //private char[] rightGuess=new char[0];
     List<Character> rightGuess = new ArrayList<>();
     @SuppressWarnings("FieldMayBeFinal")
     //private char[] wrongGuess=new char[allowedWrongs];
     List<Character> wrongGuess = new ArrayList<>();
   
    public static void main(String[] args) throws FileNotFoundException, Exception {
        // TODO code application logic here
        Hangman hangman;
        hangman = new Hangman();
        hangman.numOfWords=0;
        @SuppressWarnings("UnusedAssignment")
        String[]words=new String[0];
        Scanner fileName = new Scanner(System.in);
        System.out.println("Please enter teh name of the file containing the dictionary WITH its extension!");
        words=readFile(fileName.next());
        hangman.setDictionary(words);
        hangman.gameWord = hangman.selectRandomSecretWord();
        //hangman.rightGuess= new char[hangman.gameWord.length()];
        for(int i=0;i<hangman.gameWord.length();i++)
            System.out.print("-");
        System.out.println();
        hangman.toDisplay=new char[hangman.gameWord.length()];
        System.out.println("The word is composed of "+hangman.gameWord.length()+" letters.");
        while(!hangman.lose && !hangman.win)
        {
            System.out.println("Number of wrong guesses left: "+hangman.allowedWrongs);
            System.out.println("Plese enter a charcter to guess.");
            Scanner user =new Scanner(System.in);
            hangman.guess(user.next().charAt(0));
            for(char ca : hangman.toDisplay)
            {
                if(ca>='a' && ca<='z')
                {
                System.out.print(ca);
                }
                else
                    System.out.print("-");
            }
            System.out.println();
           // if(char ca : hangman.rightGuess)
                System.out.print("Right Guesses:");
            hangman.rightGuess.forEach((ca) -> {
                System.out.printf("%c\t",ca);
            });
            System.out.println();
            //if(char ca : hangman.wrongGuess)
                System.out.print("Wrong Guesses:");
            hangman.wrongGuess.forEach((ca) -> {
                System.out.printf("%c\t",ca);
            });
            System.out.println();
            hangman.allowedWrongs--;
            if(hangman.allowedWrongs<=0)
                hangman.lose=true;
            if(hangman.rights==hangman.gameWord.length())
                    hangman.win=true;
        }
        
        if(hangman.lose)
            System.out.println("Game Over!!!\nCorrectWord was -"+hangman.gameWord+"-\nThanks for Playing!");
        if(hangman.win)
            System.out.println("Congratulations!!\nYou got the word -"+hangman.gameWord+"- right!\nThanks for Playing!");
        
        
    } 
        

    /**
     *
     * @param filename 
     * @return  
     */
    static public String[] readFile(String filename){
        File file = new File(filename);
        int wordsCount=0;
        int i;
        List<String> wordsList;
         wordsList = new ArrayList<>();
        try (Scanner wordBank = new Scanner(file)) {
            while (wordBank.hasNextLine()) {
                wordsList.add(wordBank.nextLine());
                //System.out.println(fileWords[wordsCount]);
                //System.out.println(fileWords.length+fileWords[wordsCount-1]);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Hangman.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String[] fileWords = new String[wordsList.size()];
        fileWords = wordsList.toArray(fileWords);
        //for(i=0;i<14;i++)
          //  System.out.println(fileWords[i]);
        
        return fileWords;
    }
    //private char[] toDisplay;

    /**
     *
     * @param words
     */
    @Override
    public void setDictionary(String[] words) {
        this.numOfWords=words.length;
        this.dictionary=new String[this.numOfWords];
        this.dictionary=words;
        
        //for(String word : words )
          //  System.out.println(word);
        
       
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String selectRandomSecretWord() {
        Random ran = new Random();
        int x = ran.nextInt(this.numOfWords);
        //System.out.println(x);
        //this.rightGuess= char[]
        return this.dictionary[x];
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void guess(Character c) throws Exception {
        boolean flag=false;
        for(char ca : this.rightGuess)
        {
            if(ca==c)
            {
                System.out.println("You already correctly guessed this letter before!!\nPlease try again.");
                flag=true;
                break;
            }
        }
        
        for(char ca : this.wrongGuess)
        {
            if(ca==c)
            {
                System.out.println("You already incorrectly guessed this letter before!!\nPlease try again.");
                flag=true;
                this.allowedWrongs++;
                break;
            }
        }
        //char []toReturn;
         //toReturn = new char[this.gameWord.length()];
        /*for(int i=0;i<this.gameWord.length();i++)
        {
            for(char ca : rightGuess)
        {
            if(this.gameWord.indexOf(ca)>0)
                this.toDisplay[this.gameWord.indexOf(ca)]=ca;
            else
                toDisplay[i]='-';
        }
        }*/
        int lastInd=-1;
        for(int i=0;i<this.gameWord.length();i++)
        {
        if(this.gameWord.indexOf(c,0+ lastInd+1)>=0)
            {
                
                lastInd=this.gameWord.indexOf(c,0+ lastInd);
                this.toDisplay[this.gameWord.indexOf(c)]=c;
                if(!flag)
                {
                    this.rights++;
                    this.rightGuess.add(c);
                }
                    this.allowedWrongs++;
                if(!flag)
                    System.out.println("Correct guess!!");
                //this.rightGuess[this.rights++]=c;
                        
            }
        }
            if(this.gameWord.indexOf(c)<0)
        {
            if(!flag)
                this.wrongGuess.add(c);
        }
        
        //return Arrays.toString(toReturn);
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

 /*   @Override
    public void setMaxWrongGuesses(Integer max) {
        if(max!=null)
        {
        this.allowedWrongs=max;
        } else {
            this.allowedWrongs=1;
         }
        
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
    
        
    }
